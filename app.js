const express = require('express')
let dotenv = require("dotenv");
let path = require("path");
const app = express()
const port = 3000
const envFile = process.env.ENV_NAME ? `${process.env.ENV_NAME}.env` : "dev.env"
dotenv.config({ path: path.join(__dirname, './config', envFile) });
const schedulerUtil = require('./app/utils/schedulerUtil');

app.use(function(req, res, next) {
  const allowedOrigins = ['https://megaintelligence.net','https://www.megaintelligence.net','http://localhost:4200'];
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin)) {
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

app.use( "/health", function(req, res) {
  res.send("UP")
});

require("./app/routes/predict.routes")(app);
require("./app/routes/dashboard.routes")(app);
require("./app/routes/config.routes")(app);
require("./app/routes/scrapper.routes")(app);

app.listen(port, process.env.PRIVATE_IP_ADDRESS,  () => {
  console.log(`Example app listening at http://${process.env.PRIVATE_IP_ADDRESS}:${port}`)
})

const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 , host: process.env.PRIVATE_IP_ADDRESS});

wss.on('connection', function connection(ws, request, client) {
  ws.on('message', function message(msg) {
    console.log(`Received message ${msg} from user ${client}`);
    wss.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(msg);
      }
    });
  });
  ws.on('error', error => {
    console.log('Error: %s', error);                                          
  });
  ws.send(JSON.stringify({code: 200, message: "Success"}));
});

schedulerUtil.scrapIfNeeded.start();