const request = require('request');
const qs = require("querystring");

exports.scrapRealTimeRace = () => {
    return new Promise((resolve, reject) => {
        const options = {
            url: `http://${process.env.WEB_SCRAPPER_IP_ADDRESS}:8000/webScrapper/scrapRealTimeRace/`,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve()
                console.log("Response: " + JSON.stringify(response))
            }
        })
    })
}

exports.predictRealTimeRace = () => {
    return new Promise((resolve, reject) => {
        const options = {
            url: `http://${process.env.WEB_SCRAPPER_IP_ADDRESS}:8000/webScrapper/predictRealTimeRace/`,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve()
                console.log("Response: " + JSON.stringify(response))
            }
        })
    })
}

exports.autoUpdate = () => {
    return new Promise((resolve, reject) => {
        const options = {
            url: `http://${process.env.WEB_SCRAPPER_IP_ADDRESS}:8000/webScrapper/autoUpdate/`,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve()
                console.log("Response: " + JSON.stringify(response))
            }
        })
    })
}

exports.updateRaceChineseName = () => {
    return new Promise((resolve, reject) => {
        const options = {
            url: `http://${process.env.WEB_SCRAPPER_IP_ADDRESS}:8000/webScrapper/updateRaceChineseName/`,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve()
                console.log("Response: " + JSON.stringify(response))
            }
        })
    })
}

exports.calculateProfitWithThreshold = (version) => {
    const params = qs.stringify({version: version});
    return new Promise((resolve, reject) => {
        const options = {
            url: `http://${process.env.WEB_SCRAPPER_IP_ADDRESS}:8000/webScrapper/calculateProfitWithThreshold?${params}`,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve()
                console.log("Response: " + JSON.stringify(response))
            }
        })
    })
}

exports.updateRaceCalender = () => {
    return new Promise((resolve, reject) => {
        const options = {
            url: `http://${process.env.WEB_SCRAPPER_IP_ADDRESS}:8000/webScrapper/scrapRaceDate`,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve()
                console.log("Response: " + JSON.stringify(response))
            }
        })
    })
}