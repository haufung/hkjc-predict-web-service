const db = require('../model/db')
const { commonResponse } = require('../common/response');
const configUtil = require('../utils/configUtil.js');

exports.modelVersion = (req, res) => {
  let resp = new commonResponse();
  console.log('Entering ConfigController.modelVersion');
  db.query(`select version from config.model_version where is_ready = true;`, (err, dbRes) => {
    if (err) {
      console.error('ConfigController.modelVersion :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
      res.end();
    }
  })
}

exports.modelThreshold = (req, res) => {
  let resp = new commonResponse();
  console.log('Entering ConfigController.modelThreshold');
  db.query(`select config.model_threshold.version, threshold from config.model_threshold, config.model_version 
              where config.model_threshold.version = config.model_version.version 
              and config.model_version.is_ready = true;`, (err, dbRes) => {
    if (err) {
      console.error('ConfigController.modelThreshold :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
      res.end();
    }
  })
}

exports.latestModelVersion = (req, res) => {
  let resp = new commonResponse();
  console.log('Entering ConfigController.latestModelVersion');
  db.query(`select version from config.model_latest_version;`, (err, dbRes) => {
    if (err) {
      console.error('ConfigController.latestModelVersion :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
      res.end();
    }
  })
}

translateDataMassage = (mapFrom, mapTo) => {
  mapFrom.map(item => {
    let chiObj = {}
    let engObj = {}
    chiObj[item.code] = item.chinese
    engObj[item.code] = item.english
    Object.assign(mapTo.chinese, chiObj)
    Object.assign(mapTo.english, engObj)
  })
}

exports.translateMapping = async (req, res) => {
  let resp = new commonResponse();
  console.log('Entering ConfigController.translateMapping');
  let classObject = {
    chinese: {
    },
    english: {
    }
  }
  resp.data = {}
  var raceLocationMap = await configUtil.raceLocationTranslate()
  resp.data.raceLocation = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(raceLocationMap, resp.data.raceLocation)
  var raceClassMap = await configUtil.raceClassTranslate()
  resp.data.raceClass = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(raceClassMap, resp.data.raceClass)
  var raceDistanceMap = await configUtil.raceDistanceTranslate()
  resp.data.raceDistance = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(raceDistanceMap, resp.data.raceDistance)
  var raceGoingMap = await configUtil.raceGoingTranslate()
  resp.data.raceGoing = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(raceGoingMap, resp.data.raceGoing)
  var raceCourseMap = await configUtil.raceCourseTranslate()
  resp.data.raceCourse = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(raceCourseMap, resp.data.raceCourse)
  var horseSexMap = await configUtil.horseSexTranslate()
  resp.data.horseSex = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(horseSexMap, resp.data.horseSex)
  var raceNameMap = await configUtil.raceNameTranslate()
  resp.data.raceName = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(raceNameMap, resp.data.raceName)
  var jockeyNameMap = await configUtil.jockeyNameTranslate()
  resp.data.jockeyName = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(jockeyNameMap, resp.data.jockeyName)
  var trainerNameMap = await configUtil.trainerNameTranslate()
  resp.data.trainerName = JSON.parse(JSON.stringify(classObject))
  translateDataMassage(trainerNameMap, resp.data.trainerName)
  res.send(resp);
  res.end();
}