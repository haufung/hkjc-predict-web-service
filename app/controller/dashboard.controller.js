const db = require('../model/db');
const { commonResponse } = require('../common/response');

exports.modelPerformance = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  var version = param.version.split(".").join("_")
  console.log('Entering DashBoardController.modelPerformance');
  db.query(`select 'dnn-v${param.version}' as version, race_id, balance from predict.model_${version} order by race_id asc;`, (err, dbRes) => {
    if (err) {
      console.error('DashBoardController.modelPerformance :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}

exports.modelPerformanceThreshold = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  var version = param.version.split(".").join("_")
  console.log('Entering DashBoardController.modelPerformanceThreshold');
  db.query(`select 'dnn-v${param.version}-threshold' as version, race_id, balance_threshold as balance from predict.model_${version} order by race_id asc;`, (err, dbRes) => {
    if (err) {
      console.error('DashBoardController.modelPerformanceThreshold :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      dbRes.rows.map(object => {
        if (object.balance == null) {
          delete object.balance
        }
      })
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}

exports.modelThresholdEval = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  console.log('Entering DashBoardController.modelThresholdEval');
  db.query(`select * from predict.threshold_performance where version = $1`, [param.version], (err, dbRes) => {
    if (err) {
      console.error('DashBoardController.modelThresholdEval :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}

exports.getPastPredictWithinMonthsWithThreshold = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  var numberOfMonths = param.numberOfMonths && typeof parseFloat(param.numberOfMonths) === 'number' ? param.numberOfMonths : "12"
  var threshold = param.threshold && typeof parseFloat(param.threshold) === 'number' ? param.threshold : "0.5"
  var version = param.version.split(".").join("_")
  console.log('Entering DashBoardController.getPastPredictWithinMonthsWithThreshold');
  db.query(`select model.race.race_id, place, config.race_name_english_chinese_map.chinese_name as race_name, location, horse_number, init.init_horse.chinese_name as horse_name, win_odds, round(prediction_${version}::DECIMAL, 4) from model.records, model.race, init.init_horse, config.race_name_english_chinese_map
  where model.race.race_name_english = config.race_name_english_chinese_map.english_name
  and model.records.horse_id = init.init_horse.horse_id
  and model.records.race_id = model.race.race_id
  and predicted_win_${version} is true
  and prediction_${version} > ${threshold}
  and race_date > (NOW()::timestamptz AT TIME ZONE 'Asia/Hong_Kong') - interval '${numberOfMonths} months'
  order by model.race.race_id desc`, (err, dbRes) => {
    if (err) {
      console.error('DashBoardController.getPastPredictWithinMonthsWithThreshold :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}

exports.getAllEloByCurrentRaceId = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  var version = param.version.split(".").join("_")
  console.log('Entering DashBoardController.getAllEloByCurrentRaceId');
  db.query(`select horse_number, init.init_horse.chinese_name as horse_name, horse_elo_before::integer, jockey_name, jockey_elo_before::integer, trainer_name, trainer_elo_before::integer 
  from predict.current_records, init.init_horse
    where predict.current_records.horse_id = init.init_horse.horse_id 
    and predict.current_records.race_id = $1
    order by prediction_${version} desc`, [param.race_id], (err, dbRes) => {
    if (err) {
      console.error('DashBoardController.getAllEloByCurrentRaceId :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}

exports.getAllEloAverage = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  console.log('Entering DashBoardController.getAllEloAverage');
  db.query(`select avg(horse_elo_before)::integer as avg_horse_elo, avg(jockey_elo_before)::integer as avg_jockey_elo, avg(trainer_elo_before)::integer as avg_trainer_elo from model.records`, (err, dbRes) => {
    if (err) {
      console.error('DashBoardController.getAllEloAverage :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}