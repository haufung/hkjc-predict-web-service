const db = require('../model/db')
const { commonResponse } = require('../common/response');
const moment = require('moment-timezone');

exports.listAllCurrentRaceId = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  console.log('Entering PredictController.listAllCurrentRaceId');
  db.query(`select distinct race_id from predict.current_records, config.model_start_date where config.model_start_date.version = $1 and records_date >= config.model_start_date.date order by race_id asc;`, [param.version], (err, dbRes) => {
    if (err) {
      console.error('PredictController.listAllCurrentRaceId :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
      res.end();
    }
  })
}

exports.listAllPastRaceId = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  console.log('Entering PredictController.listAllPastRaceId');
  db.query(`SELECT distinct race_id FROM predict.past_records, config.model_start_date where config.model_start_date.version = $1 and records_date >= config.model_start_date.date order by race_id asc;`, [param.version], (err, dbRes) => {
    if (err) {
      console.error('PredictController.listAllCurrentRaceId :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
      res.end();
    }
  })
}

exports.getPastRecordsByRaceId = (req, res) => {
  console.log('Entering PredictController.getPastRecordsByRaceId');
  let resp = new commonResponse();
  var param = req.query
  var version = param.version.split(".").join("_")
  if (!param.raceId) {
    resp.message = 'missing raceId'
    resp.code = 400
    res.status(400).send(resp);
  } else {
    db.query(`SELECT chinese_name, predict.past_records.horse_id, jockey_name, draw::float, actual_weight, win_odds::float, round(prediction_${version}::DECIMAL, 4) as confidence, place FROM predict.past_records, init.init_horse where predict.past_records.horse_id = init.init_horse.horse_id and race_id = $1 order by prediction_${version} desc;`, [param.raceId], (err, dbRes) => {
      if (err) {
        console.error('PredictController.getPastRecordsByRaceId :', err);
        resp.message = 'Failed to query database'
        resp.code = 500
        res.status(500).send(resp);
      } else {
        resp.data = dbRes.rows
        res.send(resp);
      }
    })
  }
}

exports.getCurrentRecordsByRaceId = (req, res) => {
  console.log('Entering PredictController.getCurrentRecordsByRaceId');
  let resp = new commonResponse();
  var param = req.query
  var version = param.version.split(".").join("_")
  if (!param.raceId) {
    resp.message = 'missing raceId'
    resp.code = 400
    res.status(400).send(resp);
  } else {
    db.query(`SELECT horse_number, chinese_name, predict.current_records.horse_id, jockey_name, draw::float, actual_weight, win_odds, round(prediction_${version}::DECIMAL, 4) as confidence FROM predict.current_records, init.init_horse where predict.current_records.horse_id = init.init_horse.horse_id and race_id = $1 order by prediction_${version} desc;`, [param.raceId], (err, dbRes) => {
      if (err) {
        console.error('PredictController.getCurrentRecordsByRaceId :', err);
        resp.message = 'Failed to query database.'
        resp.code = 500
        res.status(500).send(resp);
      } else {
        resp.data = dbRes.rows
        res.send(resp);
      }
    })
  }
}

exports.getCurrentRaceByRaceId = (req, res) => {
  console.log('Entering PredictController.getCurrentRaceByRaceId');
  let resp = new commonResponse();
  var param = req.query
  if (!param.raceId) {
    resp.message = 'missing raceId'
    resp.code = 400
    res.status(400).send(resp);
  } else {
    db.query(`select race_id, race_date, location, race_number, class, distance, going, course, race_name_english, to_char(race_time, 'HH24:MI') as race_time from predict.current_race where race_id = $1;`, [param.raceId], (err, dbRes) => {
      if (err) {
        console.error('PredictController.getCurrentRaceByRaceId :', err);
        resp.message = 'Failed to query database.'
        resp.code = 500
        res.status(500).send(resp);
      } else {
        resp.data = dbRes.rows
        res.send(resp);
        res.end();
      }
    })
  }
}

exports.getPastRaceByRaceId = (req, res) => {
  console.log('Entering PredicttController.getPastRaceByRaceId');
  let resp = new commonResponse();
  var param = req.query
  if (!param.raceId) {
    resp.message = 'missing raceId'
    resp.code = 400
    res.status(400).send(resp);
  } else {
    db.query(`select * from predict.past_race where race_id = $1;`, [param.raceId], (err, dbRes) => {
      if (err) {
        console.error('PredictController.getPastRaceByRaceId :', err);
        resp.message = 'Failed to query database.'
        resp.code = 500
        res.status(500).send(resp);
      } else {
        resp.data = dbRes.rows
        res.send(resp);
      }
    })
  }
}

exports.getEloRatingAcc = (req, res) => {
  console.log('Entering PredicttController.getEloRatingAcc');
  let resp = new commonResponse();
  var param = req.query
  if (!param.type) {
    resp.message = 'missing type'
    resp.code = 400
    res.status(400).send(resp);
  } else {
    db.query(`select correctHightestElo::float/totalHightestElo::float as eloAcc from
        (select count(*) as correctHightestElo from model.records,(select max(${param.type}_elo_before) as hightest_elo, race_id from model.records group by race_id) as alias
        where records.race_id = alias.race_id and records.${param.type}_elo_before = alias.hightest_elo and place = '1') as alias2,
        (select count(*) as totalHightestElo from model.records,(select max(${param.type}_elo_before) as hightest_elo, race_id from model.records group by race_id) as alias
        where records.race_id = alias.race_id and records.${param.type}_elo_before = alias.hightest_elo) as alias3`, (err, dbRes) => {
      if (err) {
        console.error('PredictController.getEloRatingAcc :', err);
        resp.message = 'Failed to query database.'
        resp.code = 500
        res.status(500).send(resp);
      } else {
        resp.data = dbRes.rows
        res.send(resp);
      }
    })
  }
}

exports.getWinRateAcc = (req, res) => {
  console.log('Entering PredicttController.getWinRateAcc');
  let resp = new commonResponse();
  var param = req.query
  if (!param.type) {
    resp.message = 'missing type'
    resp.code = 400
    res.status(400).send(resp);
  } else {
    db.query(`select correctHighestWinRate::float/totalHighestWinRate::float as winRateAcc from
    (select count(*) as correctHighestWinRate from model.records,(select max(${param.type}_win_rate) as highest_win_rate, race_id from model.records group by race_id) as alias
    where records.race_id = alias.race_id and records.${param.type}_win_rate = alias.highest_win_rate and place = '1') as alias2,
    (select count(*) as totalHighestWinRate from model.records,(select max(${param.type}_win_rate) as highest_win_rate, race_id from model.records group by race_id) as alias
    where records.race_id = alias.race_id and records.${param.type}_win_rate = alias.highest_win_rate) as alias3`, (err, dbRes) => {
      if (err) {
        console.error('PredictController.getWinRateAcc :', err);
        resp.message = 'Failed to query database.'
        resp.code = 500
        res.status(500).send(resp);
      } else {
        resp.data = dbRes.rows
        res.send(resp);
      }
    })
  }
}

exports.getPlaceRateAcc = (req, res) => {
  console.log('Entering PredicttController.getPlaceRateAcc');
  let resp = new commonResponse();
  var param = req.query
  if (!param.type) {
    resp.message = 'missing type'
    resp.code = 400
    res.status(400).send(resp);
  } else {
    db.query(`select correctHighestPlaceRate::float/totalHighestPlaceRate::float as placeRateAcc from
    (select count(*) as correctHighestPlaceRate from model.records,(select max(${param.type}_place_rate) as highest_place_rate, race_id from model.records group by race_id) as alias
    where records.race_id = alias.race_id and records.${param.type}_place_rate = alias.highest_place_rate and place = '1') as alias2,
    (select count(*) as totalHighestPlaceRate from model.records,(select max(${param.type}_place_rate) as highest_place_rate, race_id from model.records group by race_id) as alias
    where records.race_id = alias.race_id and records.${param.type}_place_rate = alias.highest_place_rate) as alias3`, (err, dbRes) => {
      if (err) {
        console.error('PredictController.getPlaceRateAcc :', err);
        resp.message = 'Failed to query database.'
        resp.code = 500
        res.status(500).send(resp);
      } else {
        resp.data = dbRes.rows
        res.send(resp);
      }
    })
  }
}

exports.getPredictionThresholdAcc = (req, res) => {
  console.log('Entering PredicttController.getPredictionThresholdAcc');
  let resp = new commonResponse();
  var param = req.query
  var version = param.version.split(".").join("_")
  if (!param.threshold) {
    param.threshold = 0.5
    console.log("threshold default as 0.5")
  }
  db.query(`select alias.correct_wins::float/alias2.all_wins::float as accuracy, alias3.average_win_odds from
    (select count(*) as correct_wins from model.records where prediction_${version} > ${param.threshold} and place = '1' and predicted_win_${version} = true and records_date > (select records_date from model.records order by records_date desc limit 1) - interval '1 year') as alias,
    (select count(*) as all_wins from model.records where prediction_${version} > ${param.threshold} and predicted_win_${version} = true and records_date > (select records_date from model.records order by records_date desc limit 1) - interval '1 year') as alias2,
    (select avg(win_odds) as average_win_odds from model.records where prediction_${version} > ${param.threshold} and predicted_win_${version} = true and place = '1' and records_date > (select records_date from model.records order by records_date desc limit 1) - interval '1 year') as alias3
    `, (err, dbRes) => {
    if (err) {
      console.error('PredictController.getPredictionThresholdAcc :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}

exports.getLowestWinOddsAcc = (req, res) => {
  console.log('Entering PredicttController.getLowestWinOddsAcc');
  let resp = new commonResponse();
  db.query(`select correctLowest::float/totalLowest::float winoddsacc, alias4.average_win_odds from
  (select count(*) as correctLowest from model.records,(select min(win_odds) as lowest_win_odds, race_id from model.records where label is not null group by race_id) as alias
  where records.race_id = alias.race_id and records.win_odds = alias.lowest_win_odds and place = '1') as alias2,
  (select count(*) as totalLowest from model.records,(select min(win_odds) as lowest_win_odds, race_id from model.records where label is not null group by race_id ) as alias
  where records.race_id = alias.race_id and records.win_odds = alias.lowest_win_odds) as alias3,
(select avg(win_odds) as average_win_odds from model.records,(select min(win_odds) as lowest_win_odds, race_id from model.records where label is not null group by race_id) as alias
  where records.race_id = alias.race_id and records.win_odds = alias.lowest_win_odds and place = '1') as alias4`, (err, dbRes) => {
    if (err) {
      console.error('PredictController.getLowestWinOddsAcc :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
    }
  })
}

exports.getAllCurrentRaceIdDetails = (req, res) => {
  let resp = new commonResponse();
  console.log('Entering PredictController.listAllCurrentRaceIdDetails');
  db.query(`select race_id, race_date, location, race_number, class, distance, going, course, race_name_english, to_char(race_time, 'HH24:MI') as race_time from predict.current_race order by race_id asc;`, (err, dbRes) => {
    if (err) {
      console.error('PredictController.listAllCurrentRaceIdDetails :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      resp.data = dbRes.rows
      res.send(resp);
      res.end();
    }
  })
}

exports.getCurrentRecordsBasicInfoByRaceIdAndHorseNum = (req, res) => {
  let resp = new commonResponse();
  var param = req.query
  var raceId = param.raceId
  var horseNum = param.horseNum
  console.log('Entering PredictController.getCurrentRecordsBasicInfoByRaceIdAndHorseNum');
  db.query(`select chinese_name, horse_win_rate, horse_place_rate, 
  jockey_name, jockey_win_rate, jockey_place_rate, 
  trainer_name, trainer_win_rate, trainer_place_rate, 
  draw, actual_weight, declare_weight, weight_difference_from_last_race,
  age, horse_days_from_first_race, sex, horse_race_have_taken,
  past_place_1, past_place_2, past_place_3, past_place_4, past_place_5, past_place_6,
  days_apart_from_previous_race, first_last_place_on_same_course, second_last_place_on_same_course,
  third_last_place_on_same_course, best_finish_time_on_same_distance
  from predict.current_records, init.init_horse
  where predict.current_records.horse_id = init.init_horse.horse_id
  and race_id = $1 and horse_number = $2;`, [raceId, horseNum], (err, dbRes) => {
    if (err) {
      console.error('PredictController.getCurrentRecordsBasicInfoByRaceIdAndHorseNum :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.status(500).send(resp);
    } else {
      if (dbRes.rows && dbRes.rows.length > 0) {
        var body = dbRes.rows[0]
        var newHorseFlag = body.horse_days_from_first_race == "0" ? true : false
        var pastSixRaceplace = []
        var tmpArr = [body.past_place_1, body.past_place_2, body.past_place_3, body.past_place_4, body.past_place_5, body.past_place_6]
        tmpArr.forEach(item => 
          pastSixRaceplace.push(!item ? '-' : item)
        )
        resp.data = {
          newHorseFlag: newHorseFlag,
          horseChineseName: body.chinese_name,
          horseWinRate: parseFloat(body.horse_win_rate * 100).toFixed(1),
          horsePlaceRate: parseFloat(body.horse_place_rate * 100).toFixed(1),
          jockeyName: body.jockey_name,
          jockeyWinRate: parseFloat(body.jockey_win_rate * 100).toFixed(1),
          jockeyPlaceRate: parseFloat(body.jockey_place_rate * 100).toFixed(1),
          trainerName: body.trainer_name,
          trainerWinRate: parseFloat(body.trainer_win_rate * 100).toFixed(1),
          trainerPlaceRate: parseFloat(body.trainer_place_rate * 100).toFixed(1),
          draw: body.draw,
          actualWeight: body.actual_weight,
          declareWeight: body.declare_weight,
          weightDifferenceFromLastRace: newHorseFlag ? '-' : body.weight_difference_from_last_race,
          age: body.age,
          horseYearsFromFirstRace: parseFloat(body.horse_days_from_first_race / 365).toFixed(1),
          sex: body.sex,
          horseRaceHaveTaken: body.horse_race_have_taken,
          pastSixRaceplace: pastSixRaceplace.join("/"),
          daysApartFromPreviousRace: newHorseFlag ? '-' : body.days_apart_from_previous_race,
          pastThreePlaceOnSameTrack: [body.first_last_place_on_same_course, body.second_last_place_on_same_course, body.third_last_place_on_same_course].join("/").replace(/\NULL/g, "-"),
          bestFinishTimeOnSameDistance: body.best_finish_time_on_same_distance ? moment(body.best_finish_time_on_same_distance, 'HH:mm:ss.SS').format('m.ss.SS') : '-'
        }
      }
      res.send(resp);
      res.end();
    }
  })
}

// exports.getCurrentNumberofRace = async (req, res) => {
//   let resp = new commonResponse();
//   console.log('Entering PredictController.getCurrentNumberofRace');
//   try {
//     var dbRes = await db.query(`select count(*) from predict.current_race;`)
//     resp.data = dbRes.rows[0].count
//     res.send(resp);
//     res.end();
//   } catch (err) {
//     console.error('PredictController.getCurrentNumberofRace :', err);
//     resp.message = 'Failed to query database.'
//     resp.code = 500
//     res.send(resp);
//   }
// }