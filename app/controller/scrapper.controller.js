const { commonResponse } = require('../common/response');
const scrapperUtils = require('../utils/scrapperUtil');
const scrapperClient = require('../client/scrapper.client');
const db = require('../model/db')

exports.checkIfTodayIsRaceDate = (req, res) => {
  let resp = new commonResponse();
  console.log('Entering ScrapperController.checkIfTodayIsRaceDate');
  scrapperUtils.queryToCheckIfRaceDate().then(bol => {
    resp.data = bol
    res.send(resp);
    res.end();
  }).catch((error) => {
    console.error('ScrapperController.checkIfTodayIsRaceDate :', error);
    resp.message = 'Internal Server Error'
    resp.code = 500
    res.status(500).send(resp);
  })
}

exports.triggerScrapRealTimeRace = (req, res) => {
  let resp = new commonResponse();
  console.log('Entering ScrapperController.triggerScrapRealTimeRace');
  scrapperClient.scrapRealTimeRace().catch((error) => {
    console.error('ScrapperController.triggerScrapRealTimeRace :', error);
    resp.message = 'Internal Server Error'
    resp.code = 500
    res.status(500).send(resp);
  }).then((clientRes) => {
    resp.data = clientRes.message
    res.send(resp);
    res.end();
  })
}

exports.queryNextRaceDate = (req, res) => {
  let resp = new commonResponse();
  console.log('Entering ScrapperController.queryNextRaceDate');
  db.query(`select race_date::timestamptz AT TIME ZONE 'Asia/Hong_Kong' from "webScrapper_racecalender" where race_date > (NOW()::timestamptz AT TIME ZONE 'Asia/Hong_Kong')::date limit 1`, (err, dbRes) => {
    if (err) {
      console.error('ScrapperController.queryNextRaceDate :', err);
      resp.message = 'Failed to query database.'
      resp.code = 500
      res.send(resp);
    } else {
      resp.data = dbRes.rows[0].timezone
      res.send(resp);
      res.end();
    }
  })
}