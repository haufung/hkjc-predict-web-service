module.exports = app => {
    const configController = require("../controller/config.controller")
    app.get("/config/modelVersion", configController.modelVersion);
    app.get("/config/modelThreshold", configController.modelThreshold);
    app.get("/config/latestModelVersion", configController.latestModelVersion);
    app.get("/config/translateMapping", configController.translateMapping);
};