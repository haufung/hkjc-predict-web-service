module.exports = app => {
    const validationUtil = require("../utils/validationUtil")
    const dashBoardController = require("../controller/dashboard.controller")
    app.get("/dashboard/modelPerformance", dashBoardController.modelPerformance);
    app.get("/dashboard/modelPerformanceThreshold", validationUtil.validateModelVersion, dashBoardController.modelPerformanceThreshold);
    app.get("/dashboard/modelThresholdEval", validationUtil.validateModelVersion, dashBoardController.modelThresholdEval);
    app.get("/dashboard/getPastPredictWithinMonthsWithThreshold", validationUtil.validateModelVersion, dashBoardController.getPastPredictWithinMonthsWithThreshold);
    app.get("/dashboard/getAllEloByCurrentRaceId", validationUtil.validateModelVersion, dashBoardController.getAllEloByCurrentRaceId);
    app.get("/dashboard/getAllEloAverage", dashBoardController.getAllEloAverage);
};