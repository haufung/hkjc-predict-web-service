module.exports = app => {
    const validationUtil = require("../utils/validationUtil")
    const predictController = require("../controller/predict.controller")
    app.get("/predict/listAllCurrentRaceId", validationUtil.validateModelVersion, predictController.listAllCurrentRaceId);
    app.get("/predict/listAllPastRaceId", validationUtil.validateModelVersion, predictController.listAllPastRaceId);
    app.get("/predict/getPastRecordsByRaceId", validationUtil.validateModelVersion, predictController.getPastRecordsByRaceId);
    app.get("/predict/getCurrentRecordsByRaceId", validationUtil.validateModelVersion, predictController.getCurrentRecordsByRaceId);
    app.get("/predict/getPastRaceByRaceId", predictController.getPastRaceByRaceId);
    app.get("/predict/getCurrentRaceByRaceId", predictController.getCurrentRaceByRaceId);
    app.get("/predict/getAllCurrentRaceIdDetails", predictController.getAllCurrentRaceIdDetails);
    app.get("/predict/getEloRatingAcc", predictController.getEloRatingAcc);
    app.get("/predict/getLowestWinOddsAcc", predictController.getLowestWinOddsAcc);
    app.get("/predict/getWinRateAcc", predictController.getWinRateAcc);
    app.get("/predict/getPlaceRateAcc", predictController.getPlaceRateAcc);
    app.get("/predict/getPredictionThresholdAcc", predictController.getPredictionThresholdAcc);
    app.get("/predict/getCurrentRecordsBasicInfoByRaceIdAndHorseNum", predictController.getCurrentRecordsBasicInfoByRaceIdAndHorseNum);
    // app.get("/predict/getCurrentNumberofRace", predictController.getCurrentNumberofRace);
};