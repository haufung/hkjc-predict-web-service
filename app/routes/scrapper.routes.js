module.exports = app => {
    const scrapperController = require("../controller/scrapper.controller")
    app.get("/scrapper/checkIfTodayIsRaceDate", scrapperController.checkIfTodayIsRaceDate);
    app.get("/scrapper/triggerScrapRealTimeRace", scrapperController.triggerScrapRealTimeRace);
    app.get("/scrapper/queryNextRaceDate", scrapperController.queryNextRaceDate);
};