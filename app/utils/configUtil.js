const db = require('../model/db');

exports.queryReadyModelVersion = () => {
    return new Promise((resolve, reject) => {
        db.query(`select version from config.model_version where is_ready = true;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.queryReadyModelVersion :', err);
                reject(err)
            } else {
                var tmpArr = []
                dbRes.rows.map(versionObject => tmpArr.push(versionObject.version))
                return resolve(tmpArr)
            }
        })
    })
}

exports.raceGoingTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select code, english, chinese from config.race_going_translate;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.raceGoingTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.raceClassTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select code, english, chinese from config.race_class_translate;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.raceClassTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.raceLocationTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select code, english, chinese from config.race_location_translate;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.raceLocationTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.raceDistanceTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select code, english, chinese from config.race_distance_translate;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.raceDistanceTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.raceCourseTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select code, english, chinese from config.race_course_translate;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.raceCourseTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.horseSexTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select code, english, chinese from config.horse_sex_translate;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.horseSexTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.raceNameTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select english_name as code, english_name as english, chinese_name as chinese from config.race_name_english_chinese_map;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.raceNameTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.jockeyNameTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select * from config.jockey_name_translate where chinese is not null;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.jockeyNameTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}

exports.trainerNameTranslate = () => {
    return new Promise((resolve, reject) => {
        db.query(`select * from config.trainer_name_translate where chinese is not null;`, (err, dbRes) => {
            if (err) {
                console.error('ConfigUtil.trainerNameTranslate :', err);
                reject(err)
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}