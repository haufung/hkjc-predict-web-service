var CronJob = require('cron').CronJob;
const moment = require('moment-timezone');
const scrapperClient = require('../client/scrapper.client.js');
const scrapperUtil = require('../utils/scrapperUtil.js');
const configUtil = require('../utils/configUtil.js');


module.exports.scrapIfNeeded = new CronJob('0 0 6 * * *', async function () {
    console.log("scrapIfNeeded!")
    console.log(moment.tz("Asia/Hong_Kong").format())
    if (await scrapperUtil.checkIfNeedToUpdateCalender()) {
        console.log("updating calender!")
        await scrapperClient.updateRaceCalender()
    }
    if (await scrapperUtil.queryToCheckIfRaceDate()) {
        console.log("autoUpdate!")
        await scrapperClient.autoUpdate()
        console.log("scrapRealTimeRace!")
        await scrapperClient.scrapRealTimeRace()
        console.log("predictRealTimeRace!")
        await scrapperClient.predictRealTimeRace()
        console.log("updateRaceChineseName!")
        await scrapperClient.updateRaceChineseName()
        console.log("queryToCheckIfRaceDate!")
        var readyVersion = await configUtil.queryReadyModelVersion()
        for (let version of readyVersion) {
            console.log("calculateProfitWithThreshold! " + version)
            await scrapperClient.calculateProfitWithThreshold(version)
        }
    } else if (await scrapperUtil.queryToCheckIfRaceDateYesterDay()) {
        await scrapperClient.autoUpdate()
        var readyVersion = await configUtil.queryReadyModelVersion()
        for (let version of readyVersion) {
            console.log("calculateProfitWithThreshold! " + version)
            await scrapperClient.calculateProfitWithThreshold(version)
        }
    }
}, null, true, 'Asia/Hong_Kong');

