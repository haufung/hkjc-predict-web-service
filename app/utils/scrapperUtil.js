const db = require('../model/db')

exports.queryToCheckIfRaceDate = () => {
  return new Promise((resolve, reject) => {
    db.query(`select count(*) from "webScrapper_racecalender" where race_date = (NOW()::timestamptz AT TIME ZONE 'Asia/Hong_Kong')::date`, (err, dbRes) => {
      if (err) {
        console.error('ScrapperController.checkIfTodayIsRaceDate :', err);
        reject(err)
      } else {
        console.log(parseInt(dbRes.rows[0].count) > 0)
        resolve(parseInt(dbRes.rows[0].count) > 0)
      }
    })
  })
}

exports.queryToCheckIfRaceDateYesterDay = () => {
  return new Promise((resolve, reject) => {
    db.query(`select count(*) from "webScrapper_racecalender" where race_date = (NOW()::timestamptz AT TIME ZONE 'Asia/Hong_Kong')::date - INTERVAL '1 DAY'`, (err, dbRes) => {
      if (err) {
        console.error('ScrapperController.checkIfTodayIsRaceDate :', err);
        reject(err)
      } else {
        console.log(parseInt(dbRes.rows[0].count) > 0)
        resolve(parseInt(dbRes.rows[0].count) > 0)
      }
    })
  })
}

exports.checkIfNeedToUpdateCalender = () => {
  return new Promise((resolve, reject) => {
    db.query(`select count(*) from "webScrapper_racecalender" where race_date > (NOW()::timestamptz AT TIME ZONE 'Asia/Hong_Kong')::date`, (err, dbRes) => {
      if (err) {
        console.error('ScrapperController.checkIfNeedToUpdateCalender :', err);
        reject(err)
      } else {
        resolve(parseInt(dbRes.rows[0].count) == 0)
      }
    })
  })
}