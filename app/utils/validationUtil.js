const db = require('../model/db');
const { commonResponse } = require('../common/response');

function checkModelVersion(version) {
    return new Promise((resolve, reject) => {
        db.query(`select version from config.model_version where is_ready = true;`, (err, dbRes) => {
            if (err) {
                console.error('validateModelVersion :', err);
                reject(err)
            } else {
                var tmpArr = []
                dbRes.rows.map(versionObject => tmpArr.push(versionObject.version))
                return resolve(tmpArr.includes(version))
            }
        })
    })
}

exports.validateModelVersion = (req, res, next) => {
    let resp = new commonResponse();
    var param = req.query
    if (!param.version) {
        resp.message = 'Version not provided.'
        resp.code = 400
        res.status(400).send(resp);
    } else {
        console.log('Entering validationUil.validateModelVersion');
        checkModelVersion(param.version).then((supported) => {
            if (supported) {
                next();
            } else {
                resp.message = 'Version not supported.'
                resp.code = 400
                res.status(400).send(resp);
            }
        })
    }
}